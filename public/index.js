function getAllCats() {
    db.collection('cats').get()
    .then(snapshot => {
        snapshot.forEach(doc => {
            document.getElementById('cardGrid').innerHTML += `<a href="./shop/${doc.id}" id="${doc.id}" class="card-link"><div class="card"><img class="img" src="./assets/images-kittens/${doc.data().img}" alt=""><br><p class="name">${doc.data().name}</p><p class="price">$${doc.data().price}</p></div></a>`;
        });
    })
    .catch(function(error) {
        console.log("Error: " + error);
    });
}

function showDropdown() {
    db.collection('cats').get()
    .then(snapshot => {
        snapshot.forEach((doc, i) => {
            document.getElementById('dropdown').innerHTML += `<option>${doc.data().name}</option>`;
            console.log(i);
        })
    })
    .catch(err => console.log('Error: ' + err));
}


// Form Stepping Functions
var count = 1;

function showPage(pageID) {
    document.querySelectorAll('.add-modal').forEach(function(page){
        page.classList.remove('currentPage');
    });
    document.getElementById(pageID).classList.add('currentPage');
}

function nextPage() {
    count++;
    showPage('step' + count);

    document.getElementById('circle' + (count - 1)).style.fill = "#1a1a1a"
}

function prevPage() {
    count--;
    showPage('step' + count);

    document.getElementById('circle' + count).style.fill = "#f7f7f7"
}

function drop(e){
    e.preventDefault();
    // console.log(e.dataTransfer.files);
    alert('This feature has not yet been created. We will load a default picture for your cat.');
}

function allowDrop(e){
    e.preventDefault();
    e.dataTransfer.dropEffect = 'copy';
}