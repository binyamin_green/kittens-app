# Kittimore: A Fictional Cat-themed e-store
*A full-stack web app using nodejs, express, firebase and vanilla javascript.*
*Hosted on Heroku (Live Version is not yet up-to-date).*

## What is Kittimore?
Kittimore is a fictional online store I created to showcase my talents. It uses databases, servers and more.

### Features:
    1. Add items to database
    2. Get items from database and display them on the shop page
    3. Get specific item details

## Why Kittimore?
I have decided to make kittimore for my own benefit. I would like to learn how to update a database through a form, and this seemed like a cool, fun way to do it.

## Todo:
    [ ] Use text- and box- shadows
    [ ] Make banner and shop-card-section of equal width
    [ ] Implement Google OAuth
    [ ] Design and create checkout page & make "add to cart" effective.

## License
This app is licensed under the MIT license.