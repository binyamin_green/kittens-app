const express = require('express');
const app = express();
const path = require('path');
const bodyParser = require('body-parser');

const port = process.env.PORT || 3000;

// Initialize Firebase
const admin = require('firebase-admin');

var serviceAccount = require('./.firebase/serviceAccountKey.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://testing-ee234.firebaseio.com"
});

var db = admin.firestore();

const settings = {timestampsInSnapshots: true};
db.settings(settings);

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: false })); // support encoded bodies

// Serve main page

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '/views/index.html'));
});

// Serve about page
app.get('/about', (req, res) => {
    res.sendFile(path.join(__dirname, '/views/about.html'));
})

app.get('/shop', (req, res) => {
    res.sendFile(path.join(__dirname, '/views/shop.html'));
})

app.get('/shop/:id', (req, res) => {
    res.sendFile(path.join(__dirname, '/views/item.html'));
})

app.get('/api/:id', (req, res) => {
    db.collection('cats').doc(req.params.id).get()
        .then(doc => res.json(doc.data()));
})

app.get('/add', (req, res) => {
    res.sendFile(path.join(__dirname, '/views/add.html'));
})

app.post('/form', (req, res) => {
    var parsedID = req.body.name.toLowerCase().replace(' ', '_');

    db.collection('cats').doc(parsedID).set({
        name: req.body.name,
        price: Number(req.body.price),
        stock: Number(req.body.stock),
        details: req.body.details,
        img: 'placeholder.jpeg',
        timestamp: new Date(),
        user_generated: true
    })

    res.sendFile(path.join(__dirname, '/views/complete.html'));
})

app.use(express.static(path.join(__dirname, 'public')))

app.use(function(req, res) {
    res.status(404).send("Sorry, I can't find that!")
})

app.listen(port, () => {
    console.log(`Server started on port ${port}...`)
})